/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   mbnodecontrollernlopt2.cpp
 * 
 ******************************************************************/

#include <nlopt.hpp>
#include <iostream>

#include <Eigen/Dense>

#include <fstream>
#include <chrono>

#include <stdint.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>      
#include <time.h> 

#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "nav_msgs/Path.h"




// variabili per memorizzare traiettoria in memoria
int flagMemorizzaTraiettoria =0;
const int maxElem = 5000;
std::vector<double> MemX(maxElem);
std::vector<double> MemY(maxElem);
int Elem = 0;
std::vector<double> TargetFinale (3);

// file per traiettoria
std::ofstream fTraiettoria;

// file tempi di ottimizzazione
std::ofstream FtempiOttimizzazione;

// file tempi di CPU
std::ofstream FtempiCPU;

// file durata percorso
int ho_iniziato = 0;
std::ofstream FdurataPercorso;
auto inizioPercorso = std::chrono::steady_clock::now();
auto finePercorso = std::chrono::steady_clock::now();

// file distanza percorsa
std::ofstream FdistanzaPercorsa;
double G_distanza_percorsa = 0;

// flag  se in trajectory following flagTF=1
int flagTF =0;

// primo_target
bool primo_target=true;

// flag inizio controllore
int flagOpt=0;

//----DatiPosizione--------------------------------------
Eigen::VectorXd DPxk(2700);
int ScorriDPxk=3;
Eigen::VectorXd tempxk(2700);

//-----VarGlobali---------------------------------------------
double pi = 3.14159265358;

std::vector<double> G_posizione_iniziale (3);

std::vector<double> G_vel_precedente {0,0};

double G_C1, G_C2, G_C3, G_C4, G_C5, G_C6, G_C7 =0;
std::vector<double> G_C_h {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};



// dichiaro struttura dati per vincoli
typedef struct { 
  double a,b;
  } my_constraint_data;

// dichiarazione funzione oggetto nlopt
double myfunc(unsigned n, const double *du, double *grad, void *my_func_data);
// dichiarazione funzioni di vincolo di velocità nlopt
double myvconstraint1(unsigned n, const double *du, double *grad, void *data);
double myvconstraint2(unsigned n, const double *du, double *grad, void *data);
double myvconstraint3(unsigned n, const double *du, double *grad, void *data);
double myvconstraint4(unsigned n, const double *du, double *grad, void *data);
double myvconstraint5(unsigned n, const double *du, double *grad, void *data);
double myvconstraint6(unsigned n, const double *du, double *grad, void *data);
// dichiarazione funzioni di vincolo di posizione nlopt
double myp1constraint12(unsigned n, const double *du, double *grad, void *data);
double myp2constraint12(unsigned n, const double *du, double *grad, void *data);
double myp3constraint12(unsigned n, const double *du, double *grad, void *data);
double myp4constraint12(unsigned n, const double *du, double *grad, void *data);

// dichiarazione funzioni per funzione di costo
Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u);
Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts);
double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1);
double FunzioneDiCosto(Eigen::VectorXd du);


// class controller
class Controller
{
public:
  Controller(ros::NodeHandle &nh);
  // LABORATORIO
  // void ePuck_poseCallbackTEST(const geometry_msgs::PoseStamped trans);
  void ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans);
  void clockCallbackTEST(const rosgraph_msgs::Clock rclock);
  void timer1CallbackTEST(const ros::TimerEvent &);
  void targetCallback(const geometry_msgs::Pose2D target );
  void trajectoryCallback(const nav_msgs::Path trajectory);
  
protected:
  ros::Publisher commandPub;
  ros::Time robot_clock; 
};
 
Controller::Controller(ros::NodeHandle &nh)
{
  // LABORATORIO
  // commandPub = nh.advertise<geometry_msgs::Twist>("/epuck_robot_4/mobile_base/cmd_vel", 10);
  commandPub = nh.advertise<geometry_msgs::Twist>("ePuck2/cmd_vel", 10);
  ROS_INFO("..creata istanza controller");
};

void Controller::timer1CallbackTEST(const ros::TimerEvent &)
{
  geometry_msgs::Twist msg;
  
  double lb[6] ={-0.2, -0.2, -0.2, -0.2, -0.2, -0.2,};
  double ub[6] ={0.2, 0.2, 0.2, 0.2, 0.2, 0.2,};

  // creazione ottimizzatore
  nlopt_opt opt ;
  opt = nlopt_create(NLOPT_AUGLAG_EQ, 6);
  // inserimento limiti variabili di ottimizzazione
  nlopt_set_lower_bounds(opt, lb);
  nlopt_set_upper_bounds(opt, ub);
  // scelta minimizzazione della funzione data
  nlopt_set_min_objective(opt, myfunc, NULL);
  // inserimento vincoli
  my_constraint_data data[10]= { {1,1},{1,1},{1,1},{1,1},{1,1},{1,1},{1,1},{1,1},{1,1},{1,1}};
  nlopt_add_inequality_constraint(opt, myvconstraint1, &data[0], 1e-8 );
  nlopt_add_inequality_constraint(opt, myvconstraint2, &data[1], 1e-8 );
  nlopt_add_inequality_constraint(opt, myvconstraint3, &data[2], 1e-8 );
  nlopt_add_inequality_constraint(opt, myvconstraint4, &data[3], 1e-8 );
  nlopt_add_inequality_constraint(opt, myvconstraint5, &data[4], 1e-8 );
  nlopt_add_inequality_constraint(opt, myvconstraint6, &data[5], 1e-8 );
  nlopt_add_inequality_constraint(opt, myp1constraint12, &data[6], 1e-8 );
  nlopt_add_inequality_constraint(opt, myp2constraint12, &data[7], 1e-8 );
  nlopt_add_inequality_constraint(opt, myp3constraint12, &data[8], 1e-8 );
  nlopt_add_inequality_constraint(opt, myp4constraint12, &data[9], 1e-8 );

  // valore da cui l'ottimizzatore inizia ad iterare
  double du[6]= {0,0,0,0,0,0};
  double minf;
  // setto algoritmo di ottimizzazione locale
  nlopt_opt local_opt ;
  local_opt = nlopt_create(NLOPT_LD_AUGLAG_EQ, 6);
  // parametri di ottimizzazione
  nlopt_set_ftol_rel(local_opt, 1e-4);
  nlopt_set_xtol_rel(local_opt, 1e-4);
  nlopt_set_maxtime(opt, 0.099);
  nlopt_set_local_optimizer(opt, local_opt);
 
  // incremento del contatore per il vector DPxk, inserito qui per seguire il looprate 
  if(ScorriDPxk<((DPxk.size())-3)){
        ScorriDPxk=ScorriDPxk+3;
      }
  
  // aggiunta raggiungimento della posizione iniziale prima del trajectory following
  double dist_pos_in = sqrt((DPxk(ScorriDPxk)-G_posizione_iniziale[0])*(DPxk(ScorriDPxk)-G_posizione_iniziale[0])+(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1])*(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1]));
  if(flagTF==1){
    if(dist_pos_in<=0.10){
      ScorriDPxk=0;
      DPxk=tempxk;
      flagTF=0;
    }
  }
  
  // flag partenza ottimizzazione
  if (flagOpt == 1)
  {
    if (ho_iniziato == 0)   {
       ho_iniziato = 1;
       inizioPercorso = std::chrono::steady_clock::now();
    }

    // inizio misura tempo di ottimizzazione
    auto start = std::chrono::steady_clock::now();
    // inizio misura utilizzo CPU
    clock_t ct1, ct2;  
    if ((ct1 = clock ()) == -1)  
        perror ("clock");
    
    // ottimizzazione
    nlopt_result result = nlopt_optimize(opt, du, &minf);

    // fine misura utilizzo CPU
    if ((ct2 = clock ()) == -1)      
        perror ("clock");
    FtempiCPU << ct2 - ct1 << "\n";    

    // fine misura tempo di ottimizzazione
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    FtempiOttimizzazione << elapsed_seconds.count() << "\n";

    // intercettazione errori di ottimizzazione  
    if( result == 6 || result == 1 || result == 4|| result == 3){
      msg.linear.x = G_vel_precedente[0] +du[0];
      msg.angular.z = G_vel_precedente[1] +du[1];
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
    }else{
      msg.linear.x = 0;
      msg.angular.z = 0;
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
    }
    Eigen::VectorXd duoptE(6);
    duoptE << du[0], du[1], du[2], du[3], du[4], du[5];
  }
  else if (flagOpt == 0)
  {
    msg.linear.x = 0;
    msg.angular.z = 0;
    G_vel_precedente[0] = msg.linear.x;
    G_vel_precedente[1] = msg.angular.z;
  }

  // memorizzo traiettoria in memoria
  if (flagMemorizzaTraiettoria==1){
    MemX[Elem]=G_posizione_iniziale[0];
    MemY[Elem]=G_posizione_iniziale[1];
    // std::cout<<"..memorizzo posizione traiettoria!"<<std::endl;
    double deltaAng = abs(G_posizione_iniziale[2]-TargetFinale[2]);
    double delta = sqrt((G_posizione_iniziale[0]-TargetFinale[0])*(G_posizione_iniziale[0]-TargetFinale[0])+(G_posizione_iniziale[1]-TargetFinale[1])*(G_posizione_iniziale[1]-TargetFinale[1]));
    std::cout<<"delta = "<<std::to_string(delta)<<" deltaAng = "<<std::to_string(deltaAng)<<std::endl;
    if(delta <= 0.125 && deltaAng <= 0.05){
       finePercorso  = std::chrono::steady_clock::now();   
       std::chrono::duration<double> durata_percorso_in_secondi = finePercorso - inizioPercorso;
       FdurataPercorso << durata_percorso_in_secondi.count() << "\n";
       FdistanzaPercorsa << std::to_string(G_distanza_percorsa) << "\n";
       fTraiettoria.open ("OUT_nlopt_traiettoria.csv");
       flagMemorizzaTraiettoria=0;
       int k = 0;
       for (k=0; k<=Elem; k++){
          fTraiettoria << MemX[k] << ";" << MemY[k] << "\n";
          std::cout<<">>>>>>>>>>>>> SCRIVO RIGA TRAIETTORIA!!!!"<<std::endl;
       }
       fTraiettoria.close();
    }
    Elem = Elem+1;
  }
  // pubblicazione comandi di velocità
  commandPub.publish(msg);
  
  // double velLeft = 2*(G_vel_precedente[0] - (0.053 / 2.0) * G_vel_precedente[1]) / 0.040;
  // double velRight = 2*(G_vel_precedente[0] + (0.053 / 2.0) * G_vel_precedente[1]) / 0.040;
}

void Controller::ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans)
{
  // LABORATORIO
  // double x = trans.pose.position.x;
  // double y = trans.pose.position.y;
  // double z = trans.pose.orientation.z;

  double x = trans.transform.translation.x;
  double y = trans.transform.translation.y; 
  double z = trans.transform.rotation.z;
  double w = trans.transform.rotation.w;
  double teta=0;
 
  teta = (2*asin(z));
  if(teta>-pi/2 && teta< pi/2){
    teta= -teta;
  }else if (teta > pi/2 && teta <pi && w>0){
    teta=teta;
  }else if (teta >= pi/2 && teta <pi && w<0){
    teta= -teta;
  }

  if(ho_iniziato==1 && flagMemorizzaTraiettoria==1 ){
     double tmp = sqrt((G_posizione_iniziale[0]-x)*(G_posizione_iniziale[0]-x)+(G_posizione_iniziale[1]-y)*(G_posizione_iniziale[1]-y));
     G_distanza_percorsa= G_distanza_percorsa+tmp;
  }
  // disturbo di posizione
  double rx = ((rand() % 6)-3)/100;
  double ry = ((rand() % 6)-3)/100;
  double rteta = ((rand() % 6)-3)/100;
  
  x= x + rx;
  y= y + ry;
  teta= teta + rteta;
  
  // passaggio posizione a vector globale
  std::vector<double> v_pose ={x,y,teta};
  G_posizione_iniziale = v_pose;
}

// intercettazione posizione desiderata
void Controller::targetCallback(const geometry_msgs::Pose2D target){
  ScorriDPxk=0;

  double x = target.x;
  double y = target.y;
  double teta = target.theta;
  // std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
  teta = fmod(teta + pi,2*pi);
  if (teta < 0){
    teta += 2*pi;
  }
  teta= teta-pi;

  DPxk.resize(2700);

  for (int k =0 ; k <= 2697; k=k+3)
  {
    
    DPxk(k) = x;
    DPxk(k+1) = y;
    DPxk(k+2) = teta;
  }
  flagOpt = 1;
  TargetFinale={x, y, teta};
  flagMemorizzaTraiettoria=1;
  // std::cout << DPxk<<std::endl;
}

// intercettazione traiettoria desiderata
void Controller::trajectoryCallback(const nav_msgs::Path trajectory){
  ScorriDPxk=0;

  double n_punti_traiettoria=trajectory.poses.size();
  
  DPxk.resize(n_punti_traiettoria*3);
  tempxk.resize(n_punti_traiettoria*3);

  double i=0;
  for(auto pose : trajectory.poses){
     double x = pose.pose.position.x;
     double y = pose.pose.position.y;
     double qz = pose.pose.orientation.z;
     double teta = -(2*asin(qz));
     tempxk(i)=x;
     tempxk(i+1)=y;
     tempxk(i+2)=teta;
     DPxk(i)=tempxk(0);
     DPxk(i+1)=tempxk(1);
     DPxk(i+2)=tempxk(2);
     i= i+3;
  }
  double xtf= tempxk((tempxk.size()-3));
  double ytf= tempxk((tempxk.size()-2));
  double tetatf= tempxk((tempxk.size()-1));
  flagOpt = 1;
  TargetFinale={xtf, ytf, tetatf};
  flagTF=1;
  flagMemorizzaTraiettoria=1;
}

void Controller::clockCallbackTEST(const rosgraph_msgs::Clock rclock)
{
  robot_clock=rclock.clock;
}

int main(int argc, char *argv[])
{
  FtempiOttimizzazione.open ("OUT_nlopt_TempiOttimizzazione.txt");
  FtempiCPU.open ("OUT_nlopt_TempiCPU.txt");
  FdurataPercorso.open ("OUT_nlopt_DurataPercorso.txt");
  FdistanzaPercorsa.open ("OUT_nlopt_DistanzaPercorsa.txt");

  ros::init(argc, argv, "mbnodecontrollernlopt");
  ros::NodeHandle node;
  if (!ros::master::check())
    return (0);

  Controller controller(node);

  // ros::Subscriber subClock = node.subscribe("/clock", 1, &Controller::clockCallbackTEST, &controller);
  // LABORATORIO
  // ros::Subscriber subPioneer_pose = node.subscribe("/vrpn_client_node/epuck_robot_4/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subPioneer_pose = node.subscribe("/ePuck2/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subTarget = node.subscribe("/target", 1, &Controller::targetCallback, &controller);
  ros::Subscriber subTrajectory = node.subscribe("/trajectory", 1, &Controller::trajectoryCallback, &controller);
  
  ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &Controller::timer1CallbackTEST, &controller);

	// 10 Hz
	ros::Rate loop_rate(10); 
 
  ros::spin();
 
  FtempiOttimizzazione.close();
  FtempiCPU.close();
  FdurataPercorso.close();
  FdistanzaPercorsa.close();

  return 0;
}

// inizio funzione di costo
   // -----mobRobCT0-----------------------------------------------------
   Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
    {
      // modello lineare tempo-continuo di un differential drive robot
      // 3 stati (x):
      // posizione del robot lungo x (x)
      // posizione del robot lungo y (y)
      // angolo di sterzo del robot (theta)
      // 2 inputs (u):
      // velocità lineare (v_r)
      // velocità angolare (w_r)
      double theta_r = x(2);
      Eigen::VectorXd dxdt = x;
      double v_r = u(0);
      double w_r = u(1);
      dxdt(0) = cos(theta_r) * v_r;
      dxdt(1) = sin(theta_r) * v_r;
      dxdt(2) = w_r;
      Eigen::VectorXd y = x;

      return dxdt;
    };

    // ----wrapToPi--------------------------------------------------------
    double wrapToPi(double x){
    x = fmod(x + pi,2*pi);
    if (x < 0)
        x += 2*pi;
    return x - pi;
    };

    // -------------mobRobDT0----------------------------------------------
    Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
    {
      int M = 10;
      double delta = Ts / M;
      Eigen::VectorXd xk1 = xk;
      for (int i = 0; i < M; i++)
      {
        xk1 = xk1 + delta*mobRobCT0(xk1,uk); 
        
      }
      
      // chiamata wrapToPi
      xk1(2)= wrapToPi(xk1(2));
      Eigen::VectorXd yk = xk;

      return xk1;
    };

    // ----mobRobCostFCN---------------------------------------------------
    double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
    {
     /* funzione di costo di NMPC per il controllo di robot mobili

      Inputs:
      du:     variabili di ottimizzazione, dal tempo k al tempo k+M-1 come vettore colonna
      x:      stato corrente al tempo k
      Ts:     tempo di campionamento del controllore
      N:      orizzonte di predizione
      M:      orizzonte di controllo
      xref:   stati del riferimento come una matrice le cui N+1 colonne ( dal tempo 0 al tempo N ) sono i riferimenti per il sistema
      u1:     precedente output del controllore al tempo k-1
      J:      costo della funzione oggetto

      */

      // la matrice Q penalizza le deviazioni dello stato dal riferimento
      Eigen::DiagonalMatrix<double, 3> Q(12, 19, 0.1);

      // matrice del costo terminale
      Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

      // la matrice Rdu penalizza il rateo di cambiamento dell'input (du)
      Eigen::DiagonalMatrix<double, 2> Rdu(8, 6);

      // la amtrice Ru penalizza la grandezza dell'input (u)
      Eigen::DiagonalMatrix<double, 2> Ru(8, 6);

      // inizializzazione ref_k
      Eigen::VectorXd ref_k(3);
      ref_k << 0, 0, 0;

      // inizializzazione theta_d
      double theta_d = 0.0;

      // inizializzazione ek
      Eigen::VectorXd ek(3);
      ek << 0, 0, 0;

      // trasformazione di du in una matrice le cui righe sono du[k] da k a k+N, 
      // riempendo le righe dopo M con zero
      Eigen::MatrixXd alldu(N, 2);
      alldu.setZero(N, 2);
      int w = 0;
      for (int i = 0; i < M; ++i)
      {
        for (int c = 0; c < 2; ++c)
        {
          alldu(i, c) = du(w);
          w++;
        }
      }

      // calcolo del costo
      // inserimento stato iniziale e costo

      //xk = x0;
      Eigen::VectorXd xk(3);
      xk << 0, 0, 0;
      for (int i = 0; i < 3; i++)
      {
        xk(i) = x0(i);
      }

      //uk = u1;
      Eigen::VectorXd uk(2);
      uk << 0, 0;
      for (int i = 0; i < 2; i++)
      {
        uk(i) = u1(i);
      }

      //init J
      double j = 0;
      
      //costo primi N istanti
      for (int i = 0; i < N; i++)
      {
        Eigen::VectorXd du_k(2);
        du_k << 0, 0;
        for (int j = 0; j < 2; j++)
        {
          du_k(j) = alldu(i, j);
        }
        du_k = du_k.transpose();
        uk = uk + du_k;

        for (int j = 0; j < 3; j++)
        {
          ref_k(j) = xref(i, j);
        }
        ref_k = ref_k.transpose();
        theta_d = ref_k(2);

        //Rot
        Eigen::MatrixXd Rot(3, 3);
        Rot.setZero(3, 3);
        Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

        // calcolo ek
        ek = Rot * (xk - ref_k);

        // calcolo costo
        j = j + ek.transpose() * Q * ek;
        j = j + du_k.transpose() * Rdu * du_k;
        j = j + uk.transpose() * Ru * uk;

        // ottengo stato al prossimo istante di predizione
        xk = mobRobDT0(xk, uk, Ts);
      }

      // costo terminale
      for (int j = 0; j < 3; j++)
      {
        ref_k(j) = xref(N, j);
      }

      ref_k = ref_k.transpose();

      theta_d = ref_k(2);

      Eigen::MatrixXd Rot(3, 3);
      Rot.setZero(3, 3);
      Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
      ek = Rot * (xk - ref_k);

      j = j + ek.transpose() * P * ek;
      //std::cout << "J = " <<std::to_string(j) << std::endl;

      return j;
    };

    double FunzioneDiCosto(Eigen::VectorXd du)
    {
      int M = 3;
      int N = 15;

      Eigen::VectorXd x0(3);
      x0<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
    
      double Ts = 0.1;

      Eigen::MatrixXd xref(N + 1, 3);
      xref.setZero(N + 1, 3);

      // valorizzo il riferimento
      for (int i = 0; i < N+1; ++i)
      {
        for (int c = 0; c < 3; ++c)
        {
          if(ScorriDPxk+c+3*i<=(DPxk.size()-4)){
            xref(i,c)=DPxk(ScorriDPxk+c+3*i);
          } else {
            xref(i,c)=DPxk((DPxk.size()-3)+c);

          }
        }
      }
     
      Eigen::VectorXd u1(2);
      u1<<G_vel_precedente[0],G_vel_precedente[1];

      // ottengo J
      double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
      return j;
    };
    // fine funzione di costo

    double myfunc(unsigned n, const double *du, double *grad, void *my_func_data)
    {
      double h = 0.001;

      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // derivata approssimata rispetto a du(0) 
      double FC = FunzioneDiCosto(duE);
      duE(0) = duE(0) + h;
      double FC_h = FunzioneDiCosto(duE);
      double derivata_approssimata_u11 = (FC_h - FC) / h;
      duE(0) = duE(0) - h;
      // derivata approssimata rispetto a du(1)  
      duE(1) = duE(1) + h;
      FC_h = FunzioneDiCosto(duE);
      double derivata_approssimata_u12 = (FC_h - FC)/ h;
      duE(1) = duE(1) - h;
      // derivata approssimata rispetto a du(2)  
      duE(2) = duE(2) + h;
      FC_h = FunzioneDiCosto(duE);
      double derivata_approssimata_u21 = (FC_h - FC) / h;
      duE(2) = duE(2) - h;
      // derivata approssimata rispetto a du(3)  
      duE(3) = duE(3) + h;
      FC_h = FunzioneDiCosto(duE);
      double derivata_approssimata_u22 = (FC_h - FC) / h;
      duE(3) = duE(3) - h;
      // derivata approssimata rispetto a du(4)  
      duE(4) = duE(4) + h;
      FC_h = FunzioneDiCosto(duE);
      double derivata_approssimata_u31 = (FC_h - FC) / h;
      duE(4) = duE(4) - h;
      // derivata approssimata rispetto a du(5)  
      duE(5) = duE(5) + h;
      FC_h = FunzioneDiCosto(duE);
      duE(5) = duE(5) - h;
      double derivata_approssimata_u32 = (FC_h - FC) / h;
  
      if (grad)
      {
        grad[0] = derivata_approssimata_u11;
        grad[1] = derivata_approssimata_u12;
        grad[2] = derivata_approssimata_u21;
        grad[3] = derivata_approssimata_u22;
        grad[4] = derivata_approssimata_u31;
        grad[5] = derivata_approssimata_u32;
      }
      
      return FunzioneDiCosto(duE);
    }

    double myvconstraint1(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];
      
      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);

      Eigen::VectorXd u(2);
      u << v1, w1;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(0));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = d / r;
        grad[2] = 0.0;
        grad[3] = 0.0;
        grad[4] = 0.0;
        grad[5] = 0.0;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myvconstraint2(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);

      Eigen::VectorXd u(2);
      u << v1, w1;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(1));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = -d / r;
        grad[2] = 0.0;
        grad[3] = 0.0;
        grad[4] = 0.0;
        grad[5] = 0.0;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myvconstraint3(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);

      Eigen::VectorXd u(2);
      u << v2, w2;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(0));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = d / r;
        grad[2] = 1 / r;
        grad[3] = d / r;
        grad[4] = 0.0;
        grad[5] = 0.0;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myvconstraint4(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);

      Eigen::VectorXd u(2);
      u << v2, w2;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(1));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = -d / r;
        grad[2] = 1 / r;
        grad[3] = -d / r;
        grad[4] = 0.0;
        grad[5] = 0.0;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myvconstraint5(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd u(2);
      u << v3, w3;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(0));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = d / r;
        grad[2] = 1 / r;
        grad[3] = d / r;
        grad[4] = 1 / r;
        grad[5] = d / r;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myvconstraint6(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double r = 0.020;
      double d = 0.053 / 2;

      Eigen::MatrixXd M(2, 2);
      M.setZero(2, 2);
      M(0, 0) = 1 / r;
      M(0, 1) = d / r;
      M(1, 0) = 1 / r;
      M(1, 1) = -d / r;

      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd u(2);
      u << v3, w3;
      Eigen::VectorXd temp(2);
      temp = M * u;

      g(0) = abs(temp(1));

      if (grad)
      {
        grad[0] = 1 / r;
        grad[1] = -d / r;
        grad[2] = 1 / r;
        grad[3] = -d / r;
        grad[4] = 1 / r;
        grad[5] = -d / r;
      }

      return ((g(0) - 1.5 * pi));
    }

    double myp1constraint12(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double ro = 0.12;
      std::vector<double> xo (1);
      std::vector<double> yo (1);
      
      xo = {0.50};
      yo = {0.50};
    
      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);
      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);
      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd xin(3);
      xin << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];
      double T = 0.1;

      Eigen::VectorXd uk1(2);
      uk1 << v1, w1;
      Eigen::VectorXd uk2(2);
      uk2 << v2, w2;
      Eigen::VectorXd uk3(2);
      uk3 << v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);

      double x12 = xk12(0);
      double y12 = xk12(1);

      double C1 = sqrt((y12 - yo[0]) * (y12 - yo[0]) + (x12 - xo[0]) * (x12 - xo[0]));

      G_C1 = C1;
      std::vector<double> C_h {0,0,0,0,0,0};
      C_h.clear();

      for (int j = 0; j < 6; j++)
      {
        duE(j) = duE(j) + 0.001;

        double v1_h = G_vel_precedente[0] + duE(0);
        double w1_h = G_vel_precedente[1] + duE(1);
        double v2_h = G_vel_precedente[0] + duE(0) + duE(2);
        double w2_h = G_vel_precedente[1] + duE(1) + duE(3);
        double v3_h = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
        double w3_h = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h << v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h << v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h << v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);

        double x12_h = xk12_h(0);
        double y12_h = xk12_h(1);

        double C1_h = sqrt((y12_h - yo[0]) * (y12_h - yo[0]) + (x12_h - xo[0]) * (x12_h - xo[0]));

        duE(j) = duE(j) - 0.001;

        C_h.push_back(C1_h);
      }

      Eigen::VectorXd temp1(1);
      temp1 << C1;

      g(0) = temp1(0);

      double h = 0.001;

      if (grad)
      {
        //std::cout<<"inizio grad1"<<std::endl;
        grad[0] = -(C_h[0] - G_C1) / h;
        //std::cout<<grad[0]<<std::endl;
        grad[1] = -(C_h[1] - G_C1) / h;
        //std::cout<<grad[1]<<std::endl;
        grad[2] = -(C_h[2] - G_C1) / h;
        //std::cout<<grad[2]<<std::endl;
        grad[3] = -(C_h[3] - G_C1) / h;
        //std::cout<<grad[3]<<std::endl;
        grad[4] = -(C_h[4] - G_C1) / h;
        //std::cout<<grad[4]<<std::endl;
        grad[5] = -(C_h[5] - G_C1) / h;
        //std::cout<<grad[5]<<std::endl;
        //std::cout<<"fine grad"<<std::endl;
      }
        //std::cout<<(ro-g(0))<<std::endl;
        double vp = ro - g(0);

        return (vp);
      }

    double myp2constraint12(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double ro = 0.12;
      std::vector<double> xo (1);
      std::vector<double> yo (1);
      
      xo = {1.00};
      yo = {0.50};

      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);
      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);
      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd xin(3);
      xin << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];
      double T = 0.1;

      Eigen::VectorXd uk1(2);
      uk1 << v1, w1;
      Eigen::VectorXd uk2(2);
      uk2 << v2, w2;
      Eigen::VectorXd uk3(2);
      uk3 << v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);

      double x12 = xk12(0);
      double y12 = xk12(1);

      double C1 = sqrt((y12 - yo[0]) * (y12 - yo[0]) + (x12 - xo[0]) * (x12 - xo[0]));

      G_C2 = C1;
      std::vector<double> C_h {0,0,0,0,0,0};
      C_h.clear();

      for (int j = 0; j < 6; j++)
      {
        duE(j) = duE(j) + 0.001;
         
        double v1_h = G_vel_precedente[0] + duE(0);
        double w1_h = G_vel_precedente[1] + duE(1);
        double v2_h = G_vel_precedente[0] + duE(0) + duE(2);
        double w2_h = G_vel_precedente[1] + duE(1) + duE(3);
        double v3_h = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
        double w3_h = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h << v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h << v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h << v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);

        double x12_h = xk12_h(0);
        double y12_h = xk12_h(1);

        double C1_h = sqrt((y12_h - yo[0]) * (y12_h - yo[0]) + (x12_h - xo[0]) * (x12_h - xo[0]));

        duE(j) = duE(j) - 0.001;

        C_h.push_back(C1_h);
      }

      Eigen::VectorXd temp1(1);
      temp1 << C1;

      g(0) = temp1(0);

      double h = 0.001;

      if(grad){
        //std::cout<<"inizio grad2"<<std::endl;
        grad[0] = -(C_h[0] - G_C2) / h;
       // std::cout<<"grad[0]= "<<grad[0]<<std::endl;
        grad[1] = -(C_h[1] - G_C2) / h;
        //std::cout<<"grad[1]= "<<grad[1]<<std::endl;
        grad[2] = -(C_h[2] - G_C2) / h;
        //std::cout<<grad[2]<<std::endl;
        grad[3] = -(C_h[3] - G_C2) / h;
        //std::cout<<grad[3]<<std::endl;
        grad[4] = -(C_h[4] - G_C2) / h;
       // std::cout<<grad[4]<<std::endl;
        grad[5] = -(C_h[5] - G_C2) / h;
        //std::cout<<grad[5]<<std::endl;
        //std::cout<<"fine grad"<<std::endl;
      }
    
      double vp = ro - g(0);
      
      //std::cout<<"fine grad"<<std::endl;
      return (vp);
    }

    double myp3constraint12(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double ro = 0.12;
      std::vector<double> xo (1);
      std::vector<double> yo (1);
      
      xo = {1.00};
      yo = {0.75};

      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);
      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);
      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd xin(3);
      xin << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];
      double T = 0.1;

      Eigen::VectorXd uk1(2);
      uk1 << v1, w1;
      Eigen::VectorXd uk2(2);
      uk2 << v2, w2;
      Eigen::VectorXd uk3(2);
      uk3 << v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);

      double x12 = xk12(0);
      double y12 = xk12(1);

      double C1 = sqrt((y12 - yo[0]) * (y12 - yo[0]) + (x12 - xo[0]) * (x12 - xo[0]));

      G_C3 = C1;
      std::vector<double> C_h {0,0,0,0,0,0};
      C_h.clear();

      for (int j = 0; j < 6; j++)
      {
        duE(j) = duE(j) + 0.001;

        double v1_h = G_vel_precedente[0] + duE(0);
        double w1_h = G_vel_precedente[1] + duE(1);
        double v2_h = G_vel_precedente[0] + duE(0) + duE(2);
        double w2_h = G_vel_precedente[1] + duE(1) + duE(3);
        double v3_h = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
        double w3_h = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h << v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h << v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h << v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);

        double x12_h = xk12_h(0);
        double y12_h = xk12_h(1);

        double C1_h = sqrt((y12_h - yo[0]) * (y12_h - yo[0]) + (x12_h - xo[0]) * (x12_h - xo[0]));

        duE(j) = duE(j) - 0.001;

        C_h.push_back(C1_h);
      }

      Eigen::VectorXd temp1(1);
      temp1 << C1;

      g(0) = temp1(0);

      double h = 0.001;

      if (grad)
      {
        //std::cout<<"inizio grad3"<<std::endl;
        grad[0] = -(C_h[0] - G_C3) / h;
        //std::cout<<grad[0]<<std::endl;
        grad[1] = -(C_h[1] - G_C3) / h;
        //std::cout<<grad[1]<<std::endl;
        grad[2] = -(C_h[2] - G_C3) / h;
        //std::cout<<grad[2]<<std::endl;
        grad[3] = -(C_h[3] - G_C3) / h;
        //std::cout<<grad[3]<<std::endl;
        grad[4] = -(C_h[4] - G_C3) / h;
        //std::cout<<grad[4]<<std::endl;
        grad[5] = -(C_h[5] - G_C3) / h;
        //std::cout<<grad[5]<<std::endl;
        //std::cout<<"fine grad"<<std::endl;
      }
      //std::cout<<(ro-g(0))<<std::endl;
      double vp = ro - g(0);

      return (vp);
    }

    double myp4constraint12(unsigned n, const double *du, double *grad, void *data)
    {
      Eigen::VectorXd duE(6);
      duE << du[0], du[1], du[2], du[3], du[4], du[5];

      // inizializzazione vettore dei vincoli
      Eigen::VectorXd g(1);

      double ro = 0.12;
      std::vector<double> xo (1);
      std::vector<double> yo (1);
      
      xo = {1.00};
      yo = {0.25};

      double v1 = G_vel_precedente[0] + duE(0);
      double w1 = G_vel_precedente[1] + duE(1);
      double v2 = G_vel_precedente[0] + duE(0) + duE(2);
      double w2 = G_vel_precedente[1] + duE(1) + duE(3);
      double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
      double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

      Eigen::VectorXd xin(3);
      xin << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];
      double T = 0.1;

      Eigen::VectorXd uk1(2);
      uk1 << v1, w1;
      Eigen::VectorXd uk2(2);
      uk2 << v2, w2;
      Eigen::VectorXd uk3(2);
      uk3 << v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);

      double x12 = xk12(0);
      double y12 = xk12(1);

      double C1 = sqrt((y12 - yo[0]) * (y12 - yo[0]) + (x12 - xo[0]) * (x12 - xo[0]));

      G_C4 = C1;
      std::vector<double> C_h {0,0,0,0,0,0};
      C_h.clear();

      for (int j = 0; j < 6; j++)
      {
        duE(j) = duE(j) + 0.001;

        double v1_h = G_vel_precedente[0] + duE(0);
        double w1_h = G_vel_precedente[1] + duE(1);
        double v2_h = G_vel_precedente[0] + duE(0) + duE(2);
        double w2_h = G_vel_precedente[1] + duE(1) + duE(3);
        double v3_h = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
        double w3_h = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h << v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h << v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h << v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);

        double x12_h = xk12_h(0);
        double y12_h = xk12_h(1);

        double C1_h = sqrt((y12_h - yo[0]) * (y12_h - yo[0]) + (x12_h - xo[0]) * (x12_h - xo[0]));

        duE(j) = duE(j) - 0.001;

        C_h.push_back(C1_h);
      }

      Eigen::VectorXd temp1(1);
      temp1 << C1;

      g(0) = temp1(0);

      double h = 0.001;

      if (grad)
      {
        //std::cout<<"inizio grad4"<<std::endl;
        grad[0] = -(C_h[0] - G_C4) / h;
        //std::cout<<grad[0]<<std::endl;
        grad[1] = -(C_h[1] - G_C4) / h;
        //std::cout<<grad[1]<<std::endl;
        grad[2] = -(C_h[2] - G_C4) / h;
        //std::cout<<grad[2]<<std::endl;
        grad[3] = -(C_h[3] - G_C4) / h;
        //std::cout<<grad[3]<<std::endl;
        grad[4] = -(C_h[4] - G_C4) / h;
        //std::cout<<grad[4]<<std::endl;
        grad[5] = -(C_h[5] - G_C4) / h;
        //std::cout<<grad[5]<<std::endl;
        //std::cout<<"fine grad"<<std::endl;
      }
      //std::cout<<(ro-g(0))<<std::endl;
      double vp = ro - g(0);

      return (vp);
    }
